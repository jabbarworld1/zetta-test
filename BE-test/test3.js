/**
 * Direction:
 * Remove key that have null or undefined value
 *
 * Expected Result:
 * [
 *   { session_name: 'first test', classes: [{ students: [{ student_name: 'budi' }] }] },
 *   { classes: [{ class_name: 'second class', students: [{ student_name: 'adi' }] }] },
 * ]
 */
const data = [
  {
    session_name: 'first test',
    classes: [{ class_name: undefined, students: [{ student_name: 'budi' }] }],
  },
  {
    session_name: null,
    classes: [
      { class_name: 'second class', students: [{ student_name: 'adi' }] },
    ],
  },
];

function result(data) {
  // Your Code Here
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < data.length; j++) {
      for (let propName in data[i]) {
        if (data[i][propName] === null || data[i][propName] === undefined) {
          delete data[i][propName];
        }
      }
      for (let propName in data[i].classes[j]) {
        if (
          data[i].classes[j][propName] === null ||
          data[i].classes[j][propName] === undefined
        ) {
          delete data[i].classes[j][propName];
        }
      }
    }
  }

  return data;
}

console.log(result(data));
