/**
 * Direction:
 * Find prefix of the word from array of string
 *
 * Expected Result:
 * fl
 */
const words = ['flower', 'flow', 'flight'];

function result(words) {
  // Your Code Here
  let arr = [];
  for (let i = 1; i <= words[0].length; i++) {
    if (
      words[1].slice(0, i) == words[0].slice(0, i) &&
      words[0].slice(0, i) == words[2].slice(0, i)
    ) {
      arr.push(words[1].slice(0, i));
    }
  }
  return arr[arr.length - 1];
}

console.log(result(words));
