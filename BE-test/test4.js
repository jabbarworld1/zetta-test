/**
 * Direction:
 * Find missing number from the list
 *
 * Expected Result:
 * 8
 */
const numbers = [9, 6, 4, 2, 3, 5, 7, 0, 1];

function result(numbers) {
  // Your Code Here
  for (let i = 1; i <= numbers.length; i++) {
    if (numbers.indexOf(i) == -1) {
      return i;
    }
  }
}

console.log(result(numbers));
